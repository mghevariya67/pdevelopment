﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Workshop_Selector
{
    public partial class MainWindow : Window
    {
        //Constant Declarations
        readonly string[] WORKSHOP = new string[5] {"Handling Stress",
            "Time Management", "Supervision Skills", "Negotiation",
            "How to Interview" };
        readonly string[] LOCATION = new string[6] {"Austin", "Chicago"
        , "Dallas", "Orlando", "Pheonix", "Raleigh"};
        readonly decimal[] DAYS = new decimal[5] { 3, 3, 3, 5, 1 };
        readonly decimal[] REGISTRATION = new decimal[5] { 1000, 800, 1500, 1300, 500 };
        readonly decimal[] DAILYFEE = new decimal[6] { 150, 225, 175, 300, 175, 150 };

        public MainWindow()
        {
            InitializeComponent();
            //Writes the list of workshops
            for (int i = 0; i <WORKSHOP.Length; i++)
            {
                lbWorkshop.Items.Add(WORKSHOP[i]);
            }
            //Writes the list of Locations
            for(int i = 0; i < LOCATION.Length; i++)
            {
                lbLocation.Items.Add(LOCATION[i]);
                
            }
        }

        private void LbWorkshop_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            decimal registrationFee = REGISTRATION[lbWorkshop.SelectedIndex];
            lblRegistrationOutput.Content = registrationFee.ToString("C");
            if (lbLocation.SelectedIndex > -1)
            {
                decimal administrationFee = DAILYFEE[lbLocation.SelectedIndex] * DAYS[lbWorkshop.SelectedIndex];
                decimal total = registrationFee + administrationFee;
                lblAdministrativeOutput.Content = administrationFee.ToString("C");
                lblTotalOutput.Content = total.ToString("C");
            }

        }

        private void LbLocation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //checks if both selections have been made
            if (lbWorkshop.SelectedIndex > -1)
            {
                //Sets variables to what has been selected
                decimal registrationFee = REGISTRATION[lbWorkshop.SelectedIndex];
                decimal administrationFee = DAILYFEE[lbLocation.SelectedIndex] * DAYS[lbWorkshop.SelectedIndex];
                decimal total = registrationFee + administrationFee;
                //Creates Output
                lblRegistrationOutput.Content = registrationFee.ToString("C");
                lblAdministrativeOutput.Content = administrationFee.ToString("C");
                lblTotalOutput.Content = total.ToString("C");
            }
        }
        //Help Buttons
        private void BtnWorkshop_Click(object sender, RoutedEventArgs e)
        {
            string output = "";
            for (int i = 0; i < WORKSHOP.Length; i++)
            {
                output += WORKSHOP[i] + "\t\t" + DAYS[i] + "\t" + REGISTRATION[i].ToString("C") +Environment.NewLine;
            }
            MessageBox.Show("Workshop \t\tDays \tRegistration Cost" + Environment.NewLine + output, "Duration and Cost of Workshops");
        }

        private void BtnLocation_Click(object sender, RoutedEventArgs e)
        {
            string output = "";
            for (int i = 0; i < LOCATION.Length; i++)
            {
                output += LOCATION[i] + "\t\t\t"  + DAILYFEE[i].ToString("C") + Environment.NewLine;
            }
            MessageBox.Show("Workshop \t\tFee per Day " + Environment.NewLine + output, "Location and Fees");
        }

        private void BtnAdminHelp_Click(object sender, RoutedEventArgs e)
        {
            if (lbLocation.SelectedIndex > -1 && lbWorkshop.SelectedIndex > -1)
            {
                decimal dailyfee = DAILYFEE[lbLocation.SelectedIndex],
                    days = DAYS[lbWorkshop.SelectedIndex],
                    adminFee = dailyfee * days;
                    
                MessageBox.Show("Workshop\t\t" + WORKSHOP[lbWorkshop.SelectedIndex] +
                    Environment.NewLine + "Cost/day\t\t\t\t" + dailyfee.ToString("C") +
                    Environment.NewLine + "# of days\t\t\t\t" + days +
                    Environment.NewLine + "Administration Fee\t\t" + adminFee.ToString("C"), 
                    "Administration Fee Calculation");
            }
            else
            {
                MessageBox.Show("Administration Fees vary based on the work shop and city" +
                    Environment.NewLine + "Select the workshop and location help buttons for more information",
                    "Help");
            }
        }
    }
}
